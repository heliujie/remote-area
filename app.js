
import { Bmob } from './utils/bmob.js'
import { Base } from './utils/base.js'
import { User } from './utils/user-model.js'

let base = new Base()
let user = new User()
Bmob.initialize(base.bmobApplicationId, base.bmobRestApiKey)

//app.js
App({
  onLaunch: function () {
    let that = this;
    let user_openid = wx.getStorageSync('user_openid');
    if (!user_openid) {
      user.wxlogin(this.afterWxlogin);
    }
  },
  onLoad: function(){
    
  },
  // 微信登录成功之后
  afterWxlogin: function(){
    let that = this;
    // 根据code， 跟bmob要opendid
    user.getOpenidByBmob(that.afterGetOpenid)
  },
  // 获取openid后
  afterGetOpenid: function(){
    let that = this;
    user.wxGetUserInfo(function(){
      // bmob登录
      user.bmobLogin(that.afterBmobFailLogin)
    })
  },
  // 登录失败后
  afterBmobFailLogin: function(){
    user.regist(function(){
      // user.$log('注册成功')
    })
  },
  globalData: {
    
  }
})
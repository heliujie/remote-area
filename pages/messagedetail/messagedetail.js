
import { Messagedetail } from 'messagedetail-model.js';
let messagedetail = new Messagedetail()

Page({
    data: {
       type: '',
       currentid: '',
       detailinfo: null,
       needInputFlag: true,
       replyPlaceholder: '回复',
       replylist: [],
       replyBtnFlag: false
    },
    onLoad: function(option){
        messagedetail.$log('option', option)
        let type = option.type
        let id = option.id
        this.setData({
          type: type,
          currentid: id
        })
        this._loadData()
    },
    _loadData: function(cb){
      let that = this
      wx.showLoading({title: '加载中...'})
      messagedetail.getDetailInfo(this.data.type, this.data.currentid, (res) => {
          messagedetail.$log('res', res)
          res.createdtime = res.createdAt.replace(/(\d{4}-)/, '')
          that.setData({
            detailinfo: res
          })
          if (that.data.type === 'comment') {
              messagedetail.getReplylist(that.data.currentid, that.data.detailinfo.commentor.objectId, (res) => {
                res.forEach((o) => {
                  o.createdtime = o.createdAt.replace(/(\d{4}-)/, '')
                })
                that.setData({
                  replylist: res
                })
                wx.hideLoading()
              })
          } else if (that.data.type === 'reply') {
            messagedetail.getReplylist(that.data.detailinfo.comment.objectId, that.data.detailinfo.replyreceiver.objectId, (res) => {
                res.forEach((o) => {
                  o.createdtime = o.createdAt.replace(/(\d{4}-)/, '')
                })
                that.setData({
                  replylist: res
                })
                wx.hideLoading()
            })
          }
      })
    },
    replyback(e){
      let that = this
      let _acceptor = messagedetail.getDataSet(e, 'username')
      that.setData({
        replyPlaceholder: '回复' + _acceptor,
        needInputFlag: true
      })
    },
    cancelReply() {
      let that = this
      that.setData({
        replyPlaceholder: '',
        needInputFlag: false
      })
    },
    replyComment(event) {
      let that = this
      let detailinfo = that.data.detailinfo
      that.setData({
        replyBtnFlag: true
      })
      wx.showLoading({title: '回复中...'})
      if (that.data.type === 'comment') {
          messagedetail.replyWork(event, detailinfo.moodid, detailinfo.objectId, detailinfo.commentor.objectId, (res) => {
              that.cancelReply()
              // 获取旧的数量
              messagedetail.getCommentOldReplycount(that.data.currentid, (oldcount)=> {
                // 更新数量
                messagedetail.addReplycountFormComment(detailinfo.objectId, oldcount, res.objectId, (result) => {
                  // 获取我对这个人的回复数据
                  messagedetail.getReplylist(that.data.currentid, detailinfo.commentor.objectId, (res) => {
                      res.forEach((o) => {
                        o.createdtime = o.createdAt.replace(/(\d{4}-)/, '')
                      })
                      that.setData({
                        replylist: res,
                        replyBtnFlag: false
                      })
                      wx.hideLoading()
                      messagedetail.dataLoading('回复成功', 'success')
                  })
                })
              })
            })
      } else if (that.data.type === 'reply') {
        messagedetail.replyWork(event, detailinfo.mood.objectId, detailinfo.comment.objectId, detailinfo.replyer.objectId, (res) => {
              that.cancelReply()
              // 获取旧的数量
              messagedetail.getCommentOldReplycount(detailinfo.comment.objectId, (oldcount)=> {
                // 更新数量
                messagedetail.addReplycountFormComment(detailinfo.comment.objectId, oldcount, res.objectId, (result) => {
                  messagedetail.getReplylist(that.data.detailinfo.comment.objectId, that.data.detailinfo.replyreceiver.objectId, (res) => {
                      res.forEach((o) => {
                        o.createdtime = o.createdAt.replace(/(\d{4}-)/, '')
                      })
                      that.setData({
                        replylist: res,
                        replyBtnFlag: false
                      })
                      wx.hideLoading()
                      messagedetail.dataLoading('回复成功', 'success')
                  })
                })
              })
          })
      }
     
    } 
})
import { Base } from '../../utils/base.js'

class Messagedetail extends Base {
  constructor() {
    super()
  }

 getDetailInfo(type, id, cb){
    let that = this
    let _include = ''
    if (type === 'comment') {
      _include = 'commentor,comment_accepter,mood[content]'
    } else if (type === 'reply') {
      _include = 'replyreceiver,replyer,comment[content],mood[content]'
    }
    let params = {
      type: 'GET',
      url: type==='comment' ? 'comment/' + id : 'reply/' + id,
      data: {
        include: _include 
      },
      scb: function(res){
        cb && cb(res)
      }
    }
    that.$http(params)
 }

  // 对某心情的某评论进行回复
  replyWork(event, moodid, commentid, commentorid, cb) {
    let that = this
    let content = event.detail.value.replytext;
    if (that.trim(content) === '') {
      that.dataLoading('输入内容');
      return;
    }
    this.getattrInStorage('user_id', (userid) => {
        let params = {
          type: "POST",
          url: 'reply',
          data: {
            content: content,
            replyer: {
              "__type": "Pointer",
              "className": "_User",
              "objectId": userid
            },
            mood: {
              "__type": "Pointer",
              "className": "mood",
              "objectId": moodid
            },
            comment: {
              "__type": "Pointer",
              "className": "comment",
              "objectId": commentid
            },
            replyreceiver: {
              "__type": "Pointer",
              "className": "comment",
              "objectId": commentorid
            },
            status: false
          },
          scb: function (res) {
            cb && cb(res)
          }
        }
       that.$http(params)
    })
    
  }

   // 给comment表，添加对应的回复数量并创建关联
  addReplycountFormComment(commentid, oldreplycount, replyid, cb) {
    let  that = this
    let c = oldreplycount + 1
    let params = {
      type: "PUT",
      url: "comment/" + commentid,
      data: {
        replycount: c,
        reply: {
          __op: "AddRelation", 
          objects: [
            {
              "__type": "Pointer", 
              "className": "reply", 
              "objectId": replyid
            }
          ] 
        }  
      },
      scb: function(res){
        cb && cb(res)
      }
    }
    that.$http(params)
  }

  // 获取某一评论回复的数量
  getCommentOldReplycount(commentid, cb) {
    let that = this
    let params = {
      type: 'GET',
      url: 'comment/' + commentid,
      data: {
        
      },
      scb: function(res){
        cb && cb(res.replycount)
      }
    }
    that.$http(params)
  }

  getReplylist(commentid, replyreceiver, cb) {
    let that = this
    let params = {
      type: 'GET',
      url: 'reply',
      data: {
        where: {
          comment: {
            __type: "Pointer",
            className: "comment",
            objectId: commentid
          },
          replyreceiver: {
            __type: "Pointer",
            className: "_User",
            objectId: replyreceiver
          }
        },
        include: 'replyreceiver,replyer'
      },
      scb: function(res){
        cb && cb(res.results)
      }
    }
    that.$http(params)
  }

}

export { Messagedetail }
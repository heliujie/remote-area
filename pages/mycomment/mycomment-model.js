import { Base } from '../../utils/base.js'

class Mycomment extends Base {
  constructor() {
    super()
  }

  getCommentlist(cb) {
    let that = this
    that.getattrInStorage('user_id', (userid) => {
        let params = {
            type: 'GET',
            url: 'comment',
            data: {
               where: {
                 commentor: {
                   __type: "Pointer",
                   className: "_User",
                   objectId: userid
                 }
               },
               order: '-createdAt',
               include: 'commentor, comment_accepter'
            },
            scb: function(res) {
              cb && cb(res.results)
            }  
        }
        that.$http(params) 
    })
  }
}

export { Mycomment }
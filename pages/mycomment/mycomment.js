import { Mycomment } from 'mycomment-model.js';
let mycomment = new Mycomment()
Page({
    data: {
        commentlist: []
    },
    onLoad: function(){
        this._loadData()
    },
    _loadData: function(cb){
        let that = this
        wx.showLoading({
            title: '正在加载...'
        })
        mycomment.getCommentlist((rows) => {
            rows.forEach((item) => {
                item.createdtime = item.createdAt.replace(/(\d{4}-)/, '')
            })
            that.setData({
                commentlist: rows
            })
            wx.hideLoading()
            cb && cb()
        })
    },
    findDetailInfo: function(e){
        let moodid = mycomment.getDataSet(e, 'moodid')
        wx.navigateTo({
            url: '/pages/mooddetail/mooddetail?moodid=' + moodid + '&from=my'
        })
    },
    onPullDownRefresh: function () {
      this._loadData(() => {
        wx.stopPullDownRefresh()
      })
    }
})
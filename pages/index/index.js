import { Index } from 'index-model.js';
let app = getApp();

let index = new Index()

Page({
  data: {
    tipText: '',
    myMoodList: [],
    requestFlag: false,
    tapStatus: false,
    writeIocnStyle: '',
    writeIocnFixedFlag: false,
    currentUserAvatar: '',
    windowHeight: (() => {
      let _h = 0
      wx.getSystemInfo({
        success: function (res) {
          _h = res.windowHeight
        }
      })
      return _h
    })()
  },
  onLoad: function(){
    let that = this
  },
  changeToWritePage: function(){
    wx.navigateTo({
      url: '/pages/write/write'
    })
  }
})
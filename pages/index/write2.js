
import { Write } from 'write-model.js';
import { _ } from '../../utils/underscore.js';


let write = new Write();
let app = getApp();
Page({
  data: {
    cardArr: [],
    baseImageHostUrl: write.baseImageHostUrl,
    enterData: {
      enterAnimate: '-webkit-transform: translate3d(0, 100%, 0); opacity: 0;',
      preUploadUrl: '',
      textareaContent: ''
    },
    preUploadUrl: '',
    isAnonymous: false,
    dialogueRow: null,
    needChoseFlag: false,
    expressionsList: write.expressionsList,
    headerData: {
      icon: '../../../imgs/write/dialogue.png'
    }
  },
  onLoad: function() {
    // this._loadData();
  },
  // 加载数据
  _loadData: function(cb){
    let that = this;
    wx.showLoading({
      title: '正在加载...'
    })
    write.getCardsList((data) => {
        data.data.results.forEach(function(item){
          item.needChoseExperssionFlag = false;
          item.expressionslist = that.data.expressionsList;
          item.choseExpressionIcon = '';
          item.choseFlag = false;
        })
        that.setData({
          cardArr: data.data.results
        })
        wx.hideLoading();
    })
  },
  // 预览图片
  previewImage: function(event){
    let url = write.getDataSet(event, 'url');
    let urls = write.findAttrsFromArr(this.data.cardArr, 'url');
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [url] // 需要预览的图片http链接列表
    })
  },
  // 切换到输入页面
  changetoEnteringArea: function(event){
    let dialogueid = write.getDataSet(event, 'id');
    let dialogueRow = write.getRowByAttrFromArr(this.data.cardArr, 'objectId', dialogueid);
    let _enterData = {
      enterAnimate: '-webkit-transform: translate3d(0, 0, 0);); opacity: 1;',
      preUploadUrl: '',
      textareaContent: ''
    }
    this.setData({
      enterData: _enterData,
      dialogueRow: dialogueRow
    })
  },
  // 选取图片
  uploadPic: function(){
    let that = this
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], 
      sourceType: ['album', 'camera'],
      success: function (res) { 
        // write.$log('chooseImage', res)
        let tempFilePaths = res.tempFilePaths
        // 图片大小
        let previewImgSize = res.tempFiles[0]['size'];
        // 如果图片太大
        if (Math.floor(previewImgSize/1024) > 1024) {
          write.confirmModal('提示', '请选择小于1MB图片上传')
          return;
        }
        let _enterData = {
          enterAnimate: '-webkit-transform: translate3d(0, 0, 0);); opacity: 1;',
          preUploadUrl: tempFilePaths,
          textareaContent: that.data.enterData.textareaContent
        }
        that.setData({
          enterData: _enterData
        })
      }
    })
  },
  // 删掉预览图片
  delPreUrl: function(){
    let _enterData = {
      enterAnimate: '-webkit-transform: translate3d(0, 0, 0);); opacity: 1;',
      preUploadUrl: '',
      textareaContent: this.data.enterData.textareaContent
    }
    this.setData({
      enterData: _enterData
    })
  },
  // 取消输入
  cancelEntering: function(){
    let that = this
    let _enterData = {
      enterAnimate: '-webkit-transform: translate3d(0, 100%, 0);); opacity: 0;',
      textareaContent: this.data.enterData.textareaContent,
      preUploadUrl: ''
    }
    //write.$log('取消', _enterData)
    setTimeout(() => {
      that.setData({
        enterData: _enterData
      })
    }, 200)
  },
  // 输入内容的时候
  textareaContentChange(e) {
    let that = this
    let _enterData = {
      enterAnimate: '-webkit-transform: translate3d(0, 0, 0);); opacity: 1;',
      textareaContent: e.detail.value,
      preUploadUrl: that.data.enterData.preUploadUrl
    }
    that.setData({
      enterData: _enterData
    })
  },
  // 发表看法
  sendNewMood: function(event){
    let that = this
    write.publish(event, this.data.enterData, this.data.dialogueRow, function(){
      that.cancelEntering()
    });
  },
  // 显示所有表情图标
  showExpressions: function(event){
    let dialogueid = write.getDataSet(event, 'id');
    this.data.cardArr.forEach(function(item){
      if (item.objectId === dialogueid) {
        item.needChoseExperssionFlag = true;
      }
    })
    this.setData({
      cardArr: this.data.cardArr
    })
  },

  // 是否匿名切换
  switchAnonymousChange(e) {
    //write.$log('e',e)
  },

  // 隐藏所有表情图标
  closeExpressions: function(event){
    let dialogueid = write.getDataSet(event, 'id');
    this.data.cardArr.forEach(function (item) {
      if (item.objectId === dialogueid) {
        item.needChoseExperssionFlag = false;
      }
    })
    this.setData({
      cardArr: this.data.cardArr
    })
  },
  // 选择表情
  choseExpression: function (event) {
    let expressid = write.getDataSet(event, 'id');
    let expressurl = write.getDataSet(event, 'url');
    let dialogueid = write.getDataSet(event, 'dialogueid');
    let cardArr = write.changeExpressionForDialogue(expressid, expressurl, this.data.cardArr, dialogueid);

    this.setData({
      cardArr: cardArr
    })
  },

  // 收藏
  ulikeAction: function(event){
    let dialogueid = write.getDataSet(event, 'id');
    
  }
})
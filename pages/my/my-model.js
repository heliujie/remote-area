import { Base } from '../../utils/base.js'
import { Bmob } from '../../utils/bmob.js'

class My extends Base {
  constructor() {
    super()
  }

  getMyMood(cb) {
    let that = this
    let paramter = {
      url: 'mood',
      type: 'GET',
      data: {
        order: '-createdAt'
      },
      scb: function(res){
        cb && cb(res)
      }
    }
    wx.getStorage({
      key: 'user_id',
      success: function (ress) {
        paramter.data.where = { "userid": ress.data }
        that.$http(paramter)
      }
    })
  }

}

export { My }
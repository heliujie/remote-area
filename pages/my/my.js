import { My } from 'my-model.js';
let app = getApp();

let my = new My()

Page({
  data: {
    tipText: '',
    myMoodList: [],
    requestFlag: false,
    tapStatus: false,
    writeIocnStyle: '',
    writeIocnFixedFlag: false,
    currentUserAvatar: '',
    windowHeight: (() => {
      let _h = 0
      wx.getSystemInfo({
        success: function (res) {
          _h = res.windowHeight
        }
      })
      return _h
    })()
  },
   onLoad: function(){
    let that = this
    wx.showLoading({
      title: '正在加载...'
    })
    my.getattrInStorage('my_avatar', (avatar) => {
      that.setData({
        currentUserAvatar: avatar
      })
    })
    my.getMyMood((res) => {
      that.renderMoodList(res.results)
      wx.hideLoading()
    })
  },
  _loadData: function(cb){
    let that = this
    wx.showLoading({
      title: '正在加载...'
    })
    my.getMyMood((res) => {
      that.renderMoodList(res.results)
      wx.hideLoading()
      cb && cb()
    })
  },
  changeToWritePage: function(){
    wx.navigateTo({
      url: '/pages/write/write'
    })
  },
  renderMoodList: function (rows) {
    this.setData({requestFlag: true})
    let _rows = this.remakeCommentlist(rows)
    if (rows.length) {
      this.setData({
        myMoodList: _rows,
        tipText: ''
      })
    } else {
      this.setData({
        tipText: ""
      })
    }
  },
  // 重组回复数据
  remakeCommentlist: function (rows) {
    rows.forEach((item) => {
      let _arr1 = item.createdAt.replace(/(\d{4}-)/, '').split(' ');
      let _arr2 = _arr1[0].split('-')
      item.createdTime = _arr1[1].slice(0, 5)
      item.createdDay = _arr2[1]
      let createdMonth = _arr2[0]
      // if (createdMonth.charAt(0) === '0') {
      //   createdMonth = createdMonth.charAt(1)
      // } 
      item.createdMonth = createdMonth
    })
    return rows
  },
  moodDetail: function(e){
    let moodid = my.getDataSet(e, 'id')
    wx.navigateTo({
      url: '/pages/mooddetail/mooddetail?moodid=' + moodid + '&from=my'
    })
  },
  test: function(){
    // 关联
    //let param = { type: 'PUT', url: 'mood/41692e7ec7', data: { "comment": { "__op": "AddRelation", "objects": [{ "__type": "Pointer", "className": "comment", "objectId": "qI3hKKKY" }, { "__type": "Pointer", "className": "comment", "objectId": "pYs6VVVh" }] }} };
    //my.$http(param)

    // 查询关联
    let param = {
      type: 'GET', url: 'comment',
      data: {
        where: {
          $relatedTo: {
            object: {
              "__type": "Pointer", "className": "mood", "objectId": "41692e7ec7"
            },
            key: "comment"
          }
        },
        count: 1,
        limit: 0
      }
    }
    my.$http(param)
  },
  onPullDownRefresh: function () {
    this._loadData(() => {
      wx.stopPullDownRefresh()
    })
  },
  // 预览图片
  previewImage: function (event) {
    my.preview(event)
  },

  // 触摸
  changeCurrentBg: function(){
    // my.$log('a', 'a')
    this.setData({
      tapStatus: true
    })
  },

  refreshData() {
    this._loadData(() => {
      
    })
  },

  changeBackCurrentBg: function(){
    this.setData({
      tapStatus: false
    })
  },
  imageLoad(e){
    let imageSize = my.imageUtil(e) 
    let _index = my.getDataSet(e, 'index')
    this.data.myMoodList[_index]['imageWidth'] = imageSize.imageWidth
    this.data.myMoodList[_index]['imageHeight'] = imageSize.imageHeight
    this.setData({
      myMoodList: this.data.myMoodList
    }) 
  },
  scrollEvent(e) {
    let _scrollTop = e.detail.scrollTop;
    let that = this
    if (_scrollTop >= 50 && !that.data.writeIocnFixedFlag) {
      that.setData({
        writeIocnStyle: `
          position: fixed;
          left: 0;
          top: 0;
          right: 0;
          z-index: 10;
          border-bottom: 1px solid #eee;
        `,
        writeIocnFixedFlag: true
      })
    } else if (_scrollTop < 50 && that.data.writeIocnFixedFlag) {
      that.setData({
        writeIocnStyle: `
          position: 'static'
        `,
        writeIocnFixedFlag: false
      })
    }
  }
})
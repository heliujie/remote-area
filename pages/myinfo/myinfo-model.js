import { Base } from '../../utils/base.js'

class Myinfo extends Base {
  constructor() {
    super()
  }

  getMessageCount(cb) {
    let that = this
    that.getattrInStorage('user_id', (userid) => {
        let params = {
            type: 'GET',
            url: 'comment',
            data: {
               where: {
                 comment_accepter: {
                   __type: "Pointer",
                   className: "_User",
                   objectId: userid
                 },
                 commentor: {
                   $ne: userid
                 },
                 status: false
               },
               limit: 0,
               count: 1
            },
            scb: function(res) {
              cb && cb(res)
            }  
        }
        that.$http(params) 
    })
  }

  getMoodCount(cb){
    let that = this
    that.getattrInStorage('user_id', (userid) => {
      let params = {
        type: 'GET',
        url: 'mood',
        data: {
          where: {
            utterer: {
              __type: "Pointer",
              className: "_User",
              objectId: userid
            }
          },
          limit: 0,
          count: 1
        },
        scb: function (res) {
          cb && cb(res)
        }
      }
      that.$http(params)
    })
  }

  getCommentCount(cb) {
    let that = this
    that.getattrInStorage('user_id', (userid) => {
      let params = {
        type: 'GET',
        url: 'comment',
        data: {
          where: {
            commentor: {
              __type: "Pointer",
              className: "_User",
              objectId: userid
            }
          },
          limit: 0,
          count: 1
        },
        scb: function (res) {
          cb && cb(res)
        }
      }
      that.$http(params)
    })
  }

  getReplyCount(cb) {
    let that = this
    that.getattrInStorage('user_id', (userid) => {
      let params = {
        type: 'GET',
        url: 'reply',
        data: {
          where: {
            replyreceiver: {
              __type: "Pointer",
              className: "_User",
              objectId: userid
            },
            status: false
          },
          limit: 0,
          count: 1
        },
        scb: function(res){
          cb && cb(res)
        }
      }
      that.$http(params)
    })
  }

}

export { Myinfo }
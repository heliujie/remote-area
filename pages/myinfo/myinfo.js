
import { Myinfo } from 'myinfo-model.js';
let myinfo = new Myinfo()
Page({
  data: {
    currentUserAvatar: '',
    currentUsername: '',
    messageCount: 0,
    commentCount: 0,
    funclist: [
      {
        name: '我的心情',
        id: 1
      },
      {
        name: '我的消息',
        id: 2
      },
      {
        name: '我的评论',
        id: 3
      }
    ]
  },
  onLoad: function(){
    let that = this
    myinfo.getattrInStorage('my_avatar', (avatar) => {
      that.setData({
        currentUserAvatar: avatar
      })
    })
    myinfo.getattrInStorage('my_username', (username) => {
      that.setData({
        currentUsername: username
      })
    })
    that._loadData()
  },
  changetofunc: function(e){
    let typeid = myinfo.getDataSet(e, 'id')
    if (typeid === 1) {
      wx.navigateTo({
        url: '/pages/my/my'
      })
    } else if (typeid === 2) {
      wx.navigateTo({
        url: '/pages/mymessage/mymessage'
      })
    } else if (typeid === 3) {
      wx.navigateTo({
        url: '/pages/mycomment/mycomment'
      })
    }
  },
  _loadData: function(cb){
      let that = this
      wx.showLoading({
          title: '正在加载...'
      })
      myinfo.getMessageCount((res) => {
        myinfo.getReplyCount((data) => {
            let _count = res.count + data.count
            if (_count > 99) {
                _count = '99+'
            }
            that.setData({
                messageCount: _count
            })
        })  
        myinfo.getCommentCount((data) => {
          if (data.count > 99) {
            data.count = '99+'
          }
          that.setData({
            commentCount: data.count
          })
          myinfo.getMoodCount((r) => {
            if (r.count > 99) {
              r.count = '99+'
            }
            that.setData({
              moodCount: r.count
            })
            wx.hideLoading()
            cb && cb()
          })
        })
      })
      
  },
  onPullDownRefresh: function () {
    this._loadData(() => {
      wx.stopPullDownRefresh()
    })
  }
})

import { Write } from 'write-model.js';
import { _ } from '../../utils/underscore.js';

let write = new Write();
let app = getApp();

Page({
  data: {
      preUploadUrl: '',
      textareaContent: '',
      submitBtnFlag: false
  },
  sendNewMood: function(event){
    let that = this
    that.setData({
      submitBtnFlag: true
    })
    write.publishMood(event, this.data.preUploadUrl, function(){
      that.setData({
        submitBtnFlag: false
      })
      wx.switchTab({
        url: '/pages/around/around?tabindex=1',
        success: function (e) {
          // 解决不刷新问题
          var page = getCurrentPages().pop();
          if (page == undefined || page == null) return;
          page.onLoad();
        } 
      })
    });
  },
  // 选取图片
  chooseImage: function () {
    let that = this
    write.chooseImage((tempFilePaths) => {
      that.setData({
        preUploadUrl: tempFilePaths
      })
    })
  },
})

import { Base } from '../../utils/base.js'
import { Bmob } from '../../utils/bmob.js'

class Write extends Base {
  constructor() {
    super()
  }

  // 获取卡片信息
  getCardsList(cb) {
    let paramter = {
      type: 'GET',
      url: 'dialogue',
      scb: (data) => {
        cb && cb(data)
      }
    }
    this.$http(paramter);  
  }

  // 发表
  publish(event, enterData, currentDialogueRow, callback) {
    let that = this;
    // 用户所写内容
    let content = event.detail.value.content;
    // 是否匿名
    let anonymousflag = event.detail.value.anonymous;
    if (this.trim(content) === '') {
      that.dataLoading('请写入内容')
      return;
    }
    wx.showLoading({
      title: '正在提交数据...'
    })
    if (enterData.preUploadUrl !== '') {
      that.bmobImgName = new Date().getTime() + '.png';
      // 上传图片
      that.uploadImg(this.bmobImgName, enterData, function(url){
        // 保存编写的数据
        that.saveExperienceData(content, anonymousflag, enterData, currentDialogueRow, that.bmobImgName, url, function(res){
          //that.$log('保存后', res)
          wx.hideLoading ()
          that.confirmModal('提示', '提交成功', function(){
            callback && callback()
          })
        })
      })
    } else {
      // 保存编写的数据
      that.saveExperienceData(content, anonymousflag, enterData, currentDialogueRow, '', '', function (res) {
        // that.$log('保存后', res)
        wx.hideLoading()
        that.confirmModal('提示', '提交成功', function () {
          callback && callback()
        })
      })
    }
  }

  // 上传图片
  uploadImg(name, url, cb) {
    let that = this
    let file = new Bmob.File(name, url);
    file.save().then(function (res) {
      cb && cb(res.url())
    }, function (error) {
      that.$log('error', error)
    })
  }

  // 保存用户所写体验信息
  saveMoodData(content, anonymousflag, preUploadUrl, bmobImgName, url, cb) {
    let that = this
    wx.getStorage({
      key: 'user_id',
      success: function (ress) {
        let userid = ress.data;
        let paramter = {
          url: 'mood',
          type: 'POST',
          data: {
            userid: userid,
            content: content,
            anonymousflag: anonymousflag,
            picname: preUploadUrl === '' ? '' : that.bmobImgName,
            // dialogue: {
            //   "__type": "Pointer",
            //   "className": "dialogue",
            //   "objectId": currentDialogueRow.objectId
            // },
            utterer: {
              "__type": "Pointer",
              "className": "_User",
              "objectId": userid
            },
            pic: {
              __type: "File",
              group: "group1",
              filename: bmobImgName === '' ? '-1' : bmobImgName,
              url: url === '' ? '' : url
            }
          },
          scb: function (res) {
            cb && cb(res)
          },
          eb: function(err){
            that.dataLoading('提交失败')
          }
        }
        that.$http(paramter)
      }
    })
  }

  // 更改表情图标
  changeExpressionForDialogue(id, url, cardArr, dialogueid) {
    cardArr.forEach(function(item){
      if (item.objectId === dialogueid) {
        item.choseExpressionIcon = url;
        item.choseExpressionIconid = id;
        item.choseFlag = true;
        item.needChoseExperssionFlag = false;
      }
    })
    return cardArr;
  }

  
  publishMood(event, preUploadUrl, callback){
    let that = this;
    // 用户所写内容
    let content = event.detail.value.content;
    // 是否匿名
    let anonymousflag = event.detail.value.anonymous;
    if (this.trim(content) === '') {
      that.dataLoading('请输入内容')
      return;
    }
    if (content.length > 140) {
      that.dataLoading('别超过140字')
      return;
    }
    wx.showLoading({title: '正玩命提交...'})
    if (preUploadUrl !== '') {
      that.bmobImgName = new Date().getTime() + '.png';
      // 上传图片
      that.uploadImg(this.bmobImgName, preUploadUrl, function (url) {
        // 保存编写的数据
        that.saveMoodData(content, anonymousflag, preUploadUrl, that.bmobImgName, url, function (res) {
          //that.$log('保存后', res)
          wx.hideLoading()
          that.confirmModal('提示', '发表成功', function () {
            callback && callback()
          })
        })
      })
    } else {
      // 保存编写的数据
      that.saveMoodData(content, anonymousflag, preUploadUrl, '', '', function (res) {
        // that.$log('保存后', res)
        wx.hideLoading()
        that.confirmModal('提示', '提交成功', function () {
          callback && callback()
        })
      })
    }
  }


  chooseImage(cb) {
    let that = this
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['compressed'], //'original',
      sourceType: ['album', 'camera'],
      success: function (res) {
        let tempFilePaths = res.tempFilePaths
        // 图片大小
        let previewImgSize = res.tempFiles[0]['size'];
        // that.$log('size', Math.floor(previewImgSize) > 1024 * 1024)
        // 如果图片太大
        that.$log('img', res)
        if (Math.floor(previewImgSize) > 1024 * 1024) {
          that.dataLoading('请选择小于1MB图片上传')
          return;
        }
        cb && cb(tempFilePaths);
      }
    })
  }
}

export { Write }
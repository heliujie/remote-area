import { Base } from '../../utils/base.js'
import { Bmob } from '../../utils/bmob.js'

class Detail extends Base {
  constructor() {
    super()
  }

  getDetailInfoById(moodid, cb) {
    let that = this
    this.getattrInStorage('user_id', (userid) => {
      let params = {
        type: "GET",
        url: "mood/" + moodid,
        data: {
          where: {
            utterer: {
              __type: "Pointer",
              className: "_User",
              objectId: userid
            }
          },
          include: 'utterer'
        },
        scb: function(res){
          cb && cb(res)
        }
      }
      that.$http(params)
    })
  }

  // 点赞
  addSupport(acceptsupportid, moodid, userid, cb) {
    let that = this
    // 添加
    let params = {
      type: "POST",
      url: "support",
      data: {
        moodid: moodid,
        accept_supportid: acceptsupportid,
        supporter: {
          "__type": "Pointer",
          "className": "_User",
          "objectId": userid
        }
      },
      scb: function (res) {
        cb && cb(res)
      }
    }
    that.$http(params) 
  }

  checkSupportFlag(userid, moodid, cb) {
    let params = {
      type: "GET",
      url: "support",
      data: {
        where: {
          moodid: moodid
          // supporter: userid
        },
        include: 'supporter'
      },
      scb: function (res) {
        cb && cb(res.results)
      }
    }
    this.$http(params)
  }

  cancelSupport(supportid, cb) {
    let params = {
      type: 'DELETE',
      url: 'support/' + supportid,
      data: {
      },
      scb: function(res){
        cb && cb(res)
      }
    }
    this.$http(params)
  }

  // 获取评论
  getCommentList(moodid, cb) {
    let that = this
    let params = {
      type: "GET",
      url: 'comment',
      data: {
        where: {
          moodid: moodid
        },
        order: '-createdAt',
        include: 'commentor,'
      },
      scb: function(res){
        cb && cb(res.results)
      }
    }
    that.$http(params)
  }

  // 回复
  replay(e, moodid, comment_accepter, cb) {
    let that = this
    let content = e.detail.value.replytext;
    
    this.getattrInStorage('user_id', (userid) => {
      let params = {
        type: 'POST',
        url: 'comment',
        data: {
          moodid: moodid,
          content: content,
          status: false,
          replycount: 0,
          commentor: {
            "__type": "Pointer",
            "className": "_User",
            "objectId": userid
          },
          comment_accepter: {
            "__type": "Pointer",
            "className": "_User",
            "objectId": comment_accepter
          },
          mood: {
            "__type": "Pointer",
            "className": "mood",
            "objectId": moodid
          }
        },
        scb: function(res) {
          cb && cb(res)
        }
      }
      that.$http(params)
    })
  }
  
  // 删除
  delMood(moodid, cb) {
    let params = {
      type: 'DELETE',
      url: 'mood/' + moodid,
      data: {},
      scb: function (res) {
        cb && cb(res)
      }
    }
    this.$http(params)
  }

  // 删除文件
  delImageFile(url, cb) {
    // http://bmob-cdn-12877.b0.upaiyun.com/2017/07/23/0147332340effbb88072c2fab369d2b9.png
    let params = {
      actionType: 'delfile',
      type: 'DELETE',
      url: url.replace(/http:\/\/bmob-cdn-12877.b0.upaiyun.com\//, ''),
      scb: function(res){
        cb && cb(res)
      }
    }
    this.$http(params)
  }

  // 删除评论，赞
  delCommentAndSupport(moodid){
    let params = {
      type: 'DELETE',
      url: 'comment/' + moodid,
      data: {},
      scb: function (res) {
        cb && cb(res)
      }
    }
    this.$http(params)
  }

  // 给mood表添加点赞count
  setSupportCount(moodid, count, cb) {
    let that = this
    let params = {
      type: "PUT",
      url: 'mood/' + moodid,
      data: {
        support_count: count
      },
      scb: function (res) {
        cb && cb(res.results)
      }
    }
    that.$http(params)
  }
  
  // 给mood表添加评论count
  setCommentCount(moodid, count, cb) {
    let that = this
    let params = {
      type: "PUT",
      url: 'mood/' + moodid,
      data: {
        comment_count: count
      },
      scb: function (res) {
        cb && cb(res.results)
      }
    }
    that.$http(params)
  }

  // 给评论点赞
  addCommentSupport(supporter, moodid, commentid, cb){
    let that = this
    // 添加
    let params = {
      type: "POST",
      url: "comment_support",
      data: {
        mood: {
          "__type": "Pointer",
          "className": "mood",
          "objectId": moodid
        },
        supporter: {
          "__type": "Pointer",
          "className": "_User",
          "objectId": supporter
        },
        comment: {
          "__type": "Pointer",
          "className": "comment",
          "objectId": commentid
        }
      },
      scb: function (res) {
        cb && cb(res)
      }
    }
    that.$http(params) 
  }

  // 给评论添加点赞数
  addSupportListForComment(commentid,supporterid, oldSupportList, cb) {
    let that = this
    oldSupportList.push(supporterid)
    // 添加
    let params = {
      type: "PUT",
      url: "comment/" + commentid,
      data: {
        supportlist: {
          "__op": "AddUnique",
          "objects": oldSupportList
        }
      },
      scb: function (res) {
        cb && cb(res)
      }
    }
    that.$http(params) 
  }

  // 检查用户对该评论是否已点赞过
  checkCommentSupportFlag(userid, supportList) {
    let res = false;
    supportList.forEach((item) => {
      if (item === userid) {
        res = true;
      }
    })
    return res;
  }

  // 挑选热评论
  pickHotComments(commentlist) {
    let result = []
    commentlist.forEach((o) => {
      let supportlist = o.supportlist || []
      if (supportlist.length >= 5) {
        result.push(o)
      }
    })
    // 从大到小排序
    result.sort((item1, item2) => {
      let supportItem1 = item1.supportlist || []
      let supportItem2 = item2.supportlist || []
      return (supportItem2.length - supportItem1.length)
    })
    // 只取前三
    if (result.length > 5) {
      result = result.slice(0, 5)
    }
    return result;
  }

  // 检查是否能对该评论进行回复
  // 参数1： 哪条心情
  // 参数2： 哪条评论
  // 参数3： 谁评论的
  checkReplyFlag(moodid, commentid, commentorid) {
    let res = true
    this.getattrInStorage('user_id', (userid) => {
      // 如果回复的人跟评论的人是同一个人，不能评论
      if (userid === commentorid) {
        res = false
        return res
      }
    })
  }

  // 对某心情的某评论进行回复
  replyComment(event, moodid, commentid, commentorid, cb) {
    let that = this
    let content = event.detail.value.replytext;
    if (that.trim(content) === '') {
      that.dataLoading('输入内容');
      return;
    }
    this.getattrInStorage('user_id', (userid) => {
        let params = {
          type: "POST",
          url: 'reply',
          data: {
            content: content,
            status: false,
            replyer: {
              "__type": "Pointer",
              "className": "_User",
              "objectId": userid
            },
            mood: {
              "__type": "Pointer",
              "className": "mood",
              "objectId": moodid
            },
            comment: {
              "__type": "Pointer",
              "className": "comment",
              "objectId": commentid
            },
            replyreceiver: {
              "__type": "Pointer",
              "className": "comment",
              "objectId": commentorid
            }
          },
          scb: function (res) {
            cb && cb(res)
          }
        }
        that.$http(params)
    })
    
  }

  // 给comment表，添加对应的回复数量并创建关联
  addReplycountFormComment(commentid, oldreplycount, replyid, cb) {
    let  that = this
    let c = oldreplycount + 1
    let params = {
      type: "PUT",
      url: "comment/" + commentid,
      data: {
        replycount: c,
        reply: {
          __op: "AddRelation", 
          objects: [
            {
              "__type": "Pointer", 
              "className": "reply", 
              "objectId": replyid
            }
          ] 
        }  
      },
      scb: function(res){
        cb && cb(res)
      }
    }
    that.$http(params)
  }

  // 测试添加关联
  test(cb) {
    let that = this
    // 添加
    let params = {
      type: "PUT",
      url: "comment/89d796c4cb",
      data: {
        reply: {
          "__op": "AddRelation", 
          "objects": [
            {
              "__type": "Pointer", 
              "className": "reply", 
              "objectId": "7c7f0f961d" 
            }, 
            {
            
              "__type": "Pointer", 
              "className": "reply", 
              "objectId": "Ivjq777A" 
            } 
          ] 
        } 
      },
      scb: function (res) {
        // that.$log('测试',res)
        cb && cb(res)
      }
    }
    that.$http(params) 
  }

  // 查询某条评论下的所有回复
  getReplyListForComment(commentid, cb) {
    let that = this
    let params = {
      type: "GET",
      url: "reply",
      data: {
        where: {
          "$relatedTo": {
            "object": {
              "__type": "Pointer",
              "className": "comment",
              objectId: commentid
            },
            "key": "reply"
          }
        },
        include:'comment,mood,replyer,replyreceiver'
      },
      scb: function (res) {
        cb && cb(res)
      }
    }
    that.$http(params) 
  }
}

export { Detail }
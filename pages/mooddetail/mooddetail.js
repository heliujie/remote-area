import { Detail } from 'mooddetail-model.js';
let app = getApp();

let detail = new Detail()

Page({
  data: {
    username: '',
    useravatar: '',
    detailInfo: null,
    // 是否对本心情有赞过
    supportYouselfFlag: false,
    supportcount: 0,
    supportlist: [],
    requestFlag: false,
    commentlist: [],
    commentPreContent: '',
    modalOpenFlag: false,
    moodid: '',
    hotCommentlist: [],
    routeFrom: '',
    currentReplyCommentid: '',
    currentReplyCommentorid: '',
    replyInputFlag: false,
    replyPreContent: '',
    currentOldreplycount: 0,
    replyPlaceholder: '回复',
    commentBtnFlag: false,
    replyBtnFlag: false,
    afterSupportMoodAction: true,
    afterSupportCommentAction: true
  },
  onLoad: function(option){
    let that = this
    this.data.moodid = option.moodid;
    that.initData(this.data.moodid)
    this.setData({
      routeFrom: option.from
    })
    // 用户名
    detail.getattrInStorage('my_username', (username) => {
      that.setData({
        username: username
      })
    })
    // 头像
    detail.getattrInStorage('my_avatar', (avatar) => {
      that.setData({
        useravatar: avatar
      })
    })
  },
  initData: function (moodid, cb){
    let that = this
    wx.showLoading({
      title: '正在加载...'
    })
    // 获取内容
    detail.getDetailInfoById(moodid, (data) => {
      that.setData({
        detailInfo: data,
        requestFlag: true
      })
      
      // 获取当前用户当前心情评论
      detail.getCommentList(moodid, (rows) => {
        let _rows = that.remakeCommentlist(rows)
        let hotlist = detail.pickHotComments(rows)
        that.setData({
          commentlist: _rows,
          hotCommentlist: hotlist
        })
        that.checkSupportCountAndRender(() => {
          wx.hideLoading()
          cb && cb()
        });
      })
    })
    
  },

  checkSupportCountAndRender: function(cb){
    let that = this
    detail.getattrInStorage('user_id', (userid) => {
      detail.checkSupportFlag(userid, that.data.moodid, (res) => {
        wx.hideLoading()
        if (res.length) {
          let _supportYouselfFlag = false
          res.forEach((item) => {
            if (item.supporter.objectId === userid) {
              _supportYouselfFlag = true
            }
          })
          that.setData({
            supportYouselfFlag: _supportYouselfFlag,
            supportlist: res
          })
        } else {
          that.setData({
            supportYouselfFlag: false,
            supportlist: res
          })
        }
        cb && cb(res.length)
      });
    })
  },

  // 点赞此心情
  supportMood: function(e){
    let that = this
    // 心情id
    let moodid = detail.getDataSet(e, 'id')
    // 接收赞useid
    let acceptid = this.data.detailInfo.userid
    
    if (that.data.supportYouselfFlag) {
      return;
    }
    detail.getattrInStorage('user_id', (userid) => {
      // 检查当前用户对该moodid是否已经点赞
      wx.showLoading({
        title: '点赞中...'
      })
      if (!that.data.afterSupportMoodAction) {
        return;
      }
      that.setData({
        afterSupportMoodAction: false
      })
      detail.checkSupportFlag(userid, moodid, (result) => {
        // 如果存在
        if (result.length) {
          let _flag = false
          result.forEach((item) => {
            if (item.supporter.objectId === userid) {
              _flag = true
            }
          })
          if (!_flag) {
            detail.addSupport(acceptid, moodid, userid, (res) => {
              that.setData({
                afterSupportMoodAction: true,
                supportYouselfFlag: true
              })
              that.checkSupportCountAndRender((count) => {
                detail.setSupportCount(moodid, count)
              });
            })
          } else {
            that.checkSupportCountAndRender();
          }
        } else {
          detail.addSupport(acceptid, moodid, userid, (res) => {
            that.setData({
              afterSupportMoodAction: true,
              supportYouselfFlag: true
            })
            that.checkSupportCountAndRender((count) => {
              detail.setSupportCount(moodid, count)
            });
          })
        }
      })
    })
  },

  // 评论
  replayAction: function(e){
    let that = this
    if (detail.trim(e.detail.value.replytext) === '') {
      detail.dataLoading('请写入评论')
      return;
    }
    wx.showLoading({
      title: '正在提交...'
    })
    that.setData({
      commentBtnFlag: true
    })
    detail.replay(e, this.data.moodid, this.data.detailInfo.utterer.objectId, (res) => {
      // 获取当前用户当前心情评论
      detail.getCommentList(this.data.moodid, (rows) => {
        let _rows = that.remakeCommentlist(rows)
        wx.hideLoading()
        that.setData({
          commentlist: _rows,
          commentPreContent: '',
          commentBtnFlag: false
        })
        detail.setCommentCount(that.data.moodid, rows.length)
      })
    })
  },

  // 重组回复数据
  remakeCommentlist: function(rows){
    rows.forEach((item) => {
      item.createdtime = item.createdAt.replace(/(\d{4}-)/, '')
    })
    return rows
  },

  showSupportDetail: function(){
    this.setData({
      modalOpenFlag: true
    })
  },

  hideSupportDetail: function(){
    this.setData({
      modalOpenFlag: false
    })
  },

  previewImage: function (event) {
    detail.preview(event)
  },
  
  delMood: function(e){
    let that = this
    let moodid = detail.getDataSet(e, 'id')
    let url = detail.getDataSet(e, 'url')
    wx.showModal({
      title: '是否删除该心情？',
      content: '删除后将不能恢复',
      showCancel: true,
      confirmColor: "#a07c52",
      cancelColor: "#646464",
      success: function (res) {
        if (res.confirm) {
          detail.delMood(moodid, (res) => {
            detail.dataLoading('删除成功', 'success', function(){
              
              if (typeof that.data.detailInfo.pic !== 'undefined') {
                that.delImageFile(that.data.detailInfo.pic.url)
              } else {
                wx.navigateTo({
                  url: '/pages/index/index'
                })
                // 删除对应的评论和赞
                // detail.delCommentAndSupport(moodid)
              }
            })
          })
        }
      }
    })
  },

  // 删除文件
  delImageFile: function (url) {
    detail.delImageFile(url, (res) => {
        wx.navigateTo({
          url: '/pages/index/index'
        })
    })
  },

  // 给评论点赞
  supportOneComment: function(e){
    let that = this
    let commentid = detail.getDataSet(e, 'id')
    let commentIndex = detail.getDataSet(e, 'index')
    let _type = detail.getDataSet(e, 'type')
    
    detail.getattrInStorage('user_id', (userid) => {
      let _supportlist = [];
      if (_type === 'normal') {
        _supportlist = that.data.commentlist[commentIndex]['supportlist'] || []
      } else if (_type === 'hot') {
        _supportlist = that.data.hotCommentlist[commentIndex]['supportlist'] || []
      }
      // 检查用户对该评论是否已点赞过
      if (detail.checkCommentSupportFlag(userid, _supportlist)) {
        detail.dataLoading('已赞', 'success')
        return;
      }
      if (!that.data.afterSupportCommentAction) {
        return;
      }
      that.setData({
        afterSupportCommentAction:false
      })
      // 点赞
      detail.addCommentSupport(userid, that.data.moodid, commentid, (res) => {
        // 给评论添加点赞数
        detail.addSupportListForComment(commentid, userid, _supportlist, (resp) => {
          // 重新获取评论
          detail.getCommentList(that.data.moodid, (rows) => {
            // 渲染
            let _rows = that.remakeCommentlist(rows)
            let hotlist = detail.pickHotComments(rows)
            that.setData({
              commentlist: _rows,
              hotCommentlist: hotlist,
              afterSupportCommentAction: true
            })
            detail.dataLoading('点赞成功', 'success')
          })
        })
      })
    })
  },


  imageLoad: function(e){
    let imageSize = detail.imageUtil(e) 
    if (typeof this.data.detailInfo.pic !== 'undefined') {
      this.data.detailInfo.imageWidth = imageSize.imageWidth
      this.data.detailInfo.imageHeight = imageSize.imageHeight
      this.setData({
        detailInfo: this.data.detailInfo
      })
    }
  },

  // 取消回复
  cancelReply: function(){
    this.setData({
      replyPreContent: '',
      replyInputFlag: false
    })
  },

  // 唤起输入框
  awakeReplyInput: function(e){
    if (this.data.routeFrom === 'square') {
      return;
    }
    let commentid = detail.getDataSet(e, 'id')
    let commentorid = detail.getDataSet(e, 'commentorid')
    let oldreplycount = detail.getDataSet(e, 'replycount')
    let _commentIndex = detail.getDataSet(e, 'index')
    let _type = detail.getDataSet(e, 'type')
    let replyflag = detail.checkReplyFlag(this.data.moodid, commentid, commentorid)
    if (!replyflag) {
      detail.dataLoading('别回复自己')
      return;
    }
    let _list = null
    if (_type === 'hot') {
      _list = this.data.hotCommentlist
    } else if(_type === 'normal') {
      _list = this.data.commentlist
    }
    this.setData({
      currentReplyCommentid: commentid,
      currentReplyCommentorid: commentorid,
      currentReplyCommentIndex: _commentIndex,
      currentReplyCommentType: _type,
      replyInputFlag: true,
      currentOldreplycount: oldreplycount,
      replyPlaceholder: '回复' + _list[_commentIndex].commentor.username
    })
  },

  // 回复评论
  replyComment: function(e){
    // 检查是否能回复
    // 参数1： 哪条心情
    // 参数2： 哪条评论
    // 参数3:  谁接收回复
    let that = this
    wx.showLoading({
      title: '正在回复...'
    })
    that.setData({
      replyBtnFlag: true
    })
    // 回复
    detail.replyComment(e, this.data.moodid, this.data.currentReplyCommentid, this.data.currentReplyCommentorid, (res) => {
      that.cancelReply()
      // 给comment表，添加对应的回复数量并创建关联
      detail.addReplycountFormComment(this.data.currentReplyCommentid, this.data.currentOldreplycount, res.objectId, (result) => {
        
        // 重新查询该条评论的数据
        // 获取当前用户当前心情评论
        detail.getCommentList(that.data.moodid, (rows) => {
          let _rows = that.remakeCommentlist(rows)
          let hotlist = detail.pickHotComments(rows)
          that.setData({
            commentlist: _rows,
            hotCommentlist: hotlist
          })
          detail.getReplyListForComment(this.data.currentReplyCommentid, (data) => {
            if (that.data.currentReplyCommentType === 'hot') {
              that.data.hotCommentlist[that.data.currentReplyCommentIndex]['replylist'] = data.results;
              that.setData({
                replyBtnFlag: false,
                hotCommentlist: that.data.hotCommentlist
              })
            } else if (that.data.currentReplyCommentType === 'normal') {
              that.data.commentlist[that.data.currentReplyCommentIndex]['replylist'] = data.results;
              that.setData({
                replyBtnFlag: false,
                commentlist: that.data.commentlist
              })
            }
            wx.hideLoading() 
            detail.dataLoading('回复成功', 'success')
          })
        })
      })
    })
  },

  // 点击评论中的查看按钮
  viewAllReplyForOneComment: function(e){
    let commentid = detail.getDataSet(e, 'id')
    let _index = detail.getDataSet(e, 'index')
    let _type = detail.getDataSet(e, 'type')
    let that = this
    let _list = null
    
    detail.getReplyListForComment(commentid, (res) => {
      if (_type === 'hot') {
        that.data.hotCommentlist[_index]['replylist'] = res.results
        that.setData({
          hotCommentlist: that.data.hotCommentlist
        })
      } else if (_type === 'normal') {
        that.data.commentlist[_index]['replylist'] = res.results
        that.setData({
          commentlist: that.data.commentlist
        })
      }
    })
  },

  onPullDownRefresh: function () {
    this.initData(this.data.moodid, () => {
      wx.stopPullDownRefresh()
    })
  },

  test: function(){
    detail.test2(() => {
      
    })
  }
  

})
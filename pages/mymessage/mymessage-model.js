import { Base } from '../../utils/base.js'

class Mymessage extends Base {
  constructor() {
    super()
  }

  getMessagelist(cb) {
    let that = this
    that.getattrInStorage('user_id', (userid) => {
        let params = {
            type: 'GET',
            url: 'comment',
            data: {
               where: {
                 comment_accepter: {
                   __type: "Pointer",
                   className: "_User",
                   objectId: userid
                 },
                 commentor: {
                   $ne: userid
                 },
                 status: false
               },
               include: 'commentor, comment_accepter'
            },
            scb: function(res) {
              cb && cb(res.results)
            }  
        }
        that.$http(params) 
    })
  }

  getReplylist(cb) {
    let that = this
    that.getattrInStorage('user_id', (userid) => {
      let params = {
        type: 'GET',
        url: 'reply',
        data: {
          where: {
            replyreceiver: {
              __type: "Pointer",
              className: "_User",
              objectId: userid
            },
            status: false
          },
          include: 'comment,replyer,mood'
        },
        scb: function(res){
          cb && cb(res.results)
        }
      }
      that.$http(params)
    })
  } 

  changeStatus(type, id, cb) {
    let that = this, _url = ''
    if (type === 'comment') {
      _url = 'comment/' + id
    } else if (type === 'reply') {
      _url = 'reply/' + id
    }
    let params = {
      type: 'PUT',
      url: _url,
      data: {
        status: true
      },
      scb: function(res){
        cb && cb(res)
      }
    }
    that.$http(params)
  }

}

export { Mymessage }
import { Mymessage } from 'mymessage-model.js';
let mymessage = new Mymessage()
Page({
    data: {
        commentlist: [],
        replylist: []
    },
    onLoad: function(){
        this._loadData()
    },
    _loadData: function(cb){
        let that = this
        wx.showLoading({
            title: '正在加载...'
        })
        mymessage.getMessagelist((rows) => {
            rows.forEach((item) => {
                item.createdtime = item.createdAt.replace(/(\d{4}-)/, '')
            })
            that.setData({
                commentlist: rows
            })
            mymessage.getReplylist((lists)=> {
                lists.forEach((item) => {
                    item.createdtime = item.createdAt.replace(/(\d{4}-)/, '')
                })
                that.setData({
                    replylist: lists
                })
                wx.hideLoading()
                cb && cb()
            }) 
        })
        
    },
    findDetailInfo: function(e){
        // let moodid = mymessage.getDataSet(e, 'moodid')
        let type = mymessage.getDataSet(e, 'type')
        let id = mymessage.getDataSet(e, 'id')
        if (type === 'comment') {
            mymessage.changeStatus(type, id, (res) => {
                wx.navigateTo({
                    url: '/pages/messagedetail/messagedetail?type=' + type +'&id='+ id
                })   
            })
        } else if (type === 'reply') {
            mymessage.changeStatus(type, id, (res) => {
                wx.navigateTo({
                    url: '/pages/messagedetail/messagedetail?type=' + type +'&id='+ id
                })
            })
        }
        // wx.navigateTo({
        //     url: '/pages/mooddetail/mooddetail?moodid=' + moodid + '&from=my'
        // })
    },
    onPullDownRefresh: function () {
      this._loadData(() => {
        wx.stopPullDownRefresh()
      })
    }
})
import { Around } from 'around-model.js';
let app = getApp();

let around = new Around()

Page({
  data: {
    windowHeight: 0,
    tabStyle: 'position: static',
    tabStyleFixedFlag: false,
    currentTabsIndex: 0,
    requestFlag: false,
    hotMoodList: [],
    lastestMoodList: [],
    hotTotalCount: 0,
    baseLimit: 10,
    currentLimit: 10,
    currentLimit_lastest: 10
  },
  onLoad: function(option){
    let that = this
    this._loadData()
    around.getWindowHeight((h) => {
      that.setData({
        windowHeight: h
      })     
    })
    around.getHotCount((res) => {
      that.setData({
        hotTotalCount: res.count
      })
    })
  },
  _loadData: function(cb){
    let that = this
    wx.showLoading({
      title: '正在加载...'
    })
    let paramter = {
      limit: that.data.currentLimit
    }
    around.getHotMoodList(paramter, (res) => {
      that.setData({
        hotMoodList: res.results,
        requestFlag: true
      })
      wx.hideLoading();
      cb && cb()
    })
  },
  _loadData_lastest: function(cb){
    let that = this
    wx.showLoading({
      title: '正在加载...'
    })
    let paramter = {
      limit: that.data.currentLimit_lastest
    }
    around.getLastestMoodList(paramter, (res) => {
      that.setData({
        lastestMoodList: res.results,
        requestFlag: true
      })
      wx.hideLoading();
      cb && cb()
    })
  },
  scrollOkPosiiton(e) {
    let _scrollTop = e.detail.scrollTop;
    let that = this
    if (_scrollTop >= 100 && !that.data.tabStyleFixedFlag) {
      that.setData({
        tabStyle: `
          position: fixed;
          left: 0;
          top: 0;
          right: 0;
          z-index: 10;
          border-top:1px solid #eee;
        `,
        tabStyleFixedFlag: true
      })
    } else if (_scrollTop < 100 && that.data.tabStyleFixedFlag) {
      that.setData({
        tabStyle: `
          position: 'static'
        `,
        tabStyleFixedFlag: false
      })
    }
  },
  loadMore(){
    let that = this
    let nowLimit = 0
    let _currentLimit = 0
    if (that.data.currentTabsIndex === 0) {
      _currentLimit = that.data.currentLimit
    } else if (that.data.currentTabsIndex === 1){
      _currentLimit = that.data.currentLimit_lastest
    } 
    nowLimit = _currentLimit + that.data.baseLimit
    if (nowLimit > that.data.hotTotalCount) {
      // around.$log('没有更多了', '没有更多了')
      nowLimit = that.data.hotTotalCount
    }
    if (that.data.currentTabsIndex === 0) {
      around.getHotMoodList({
        limit: nowLimit
      }, (res) => {
        that.setData({
          hotMoodList: res.results,
          currentLimit: nowLimit
        })
        wx.hideLoading();
      })
    } else if (that.data.currentTabsIndex === 1) {
      around.getLastestMoodList({
        limit: nowLimit
      }, (res) => {
        that.setData({
          lastestMoodList: res.results,
          currentLimit_lastest: nowLimit
        })
        wx.hideLoading();
      })
    }
  },
  // 预览图片
  previewImage: function (event) {
    around.preview(event)
  },
  changeTab(e) {
    let index = around.getDataSet(e, 'index')
    let that = this
    that.setData({
      currentTabsIndex: index
    })
    if (index === 0) {
      that._loadData()
    } else if(index === 1) {
      that._loadData_lastest()
    }
  },
  imageLoad: function(e){
    let imageSize = around.imageUtil(e) 
    let _index = around.getDataSet(e, 'index')
    this.data.hotMoodList[_index]['imageWidth'] = imageSize.imageWidth
    this.data.hotMoodList[_index]['imageHeight'] = imageSize.imageHeight
    this.setData({
      hotMoodList: this.data.hotMoodList
    })
  },
  refreshData: function(){
    let that = this
    wx.showLoading({
      title: '正在加载...'
    })
    if (that.data.currentTabsIndex === 0) {
      that._loadData(() => {
        around.getWindowHeight((h) => {
          that.setData({
            windowHeight: h
          })
        })
        wx.hideLoading()
      })
    } else if (that.data.currentTabsIndex === 1) {
      that._loadData_lastest(() => {
        around.getWindowHeight((h) => {
          that.setData({
            windowHeight: h
          })
        })
        wx.hideLoading()
      })
    }

    
  },
  viewDetailMood(e) {
    let moodid = around.getDataSet(e, 'id')
    wx.navigateTo({
      url: '/pages/mooddetail/mooddetail?moodid=' + moodid + '&from=square'
    })
  }
})
import { Base } from '../../utils/base.js'
import { Bmob } from '../../utils/bmob.js'

class Around extends Base {
  constructor() {
    super()
  }

  getHotMoodList(paramter, cb) {
    let that = this
    let params = {
      type: "GET",
      url: "mood",
      data: {
        order: '-comment_count',
        include: 'utterer',
        limit: paramter.limit
      },
      scb: function (res) {
        cb && cb(res)
      }
    }
    this.$http(params)
  }


  getLastestMoodList(paramter, cb) {
    let params = {
      type: "GET",
      url: "mood",
      data: {
        order: '-createdAt',
        include: 'utterer',
        limit: paramter.limit
      },
      scb: function (res) {
        cb && cb(res)
      }
    }
    this.$http(params)
  }

  getHotCount(cb) {
    let params = {
      type: "GET",
      url: "mood",
      data: {
        order: '-comment_count',
        include: 'utterer',
        limit: 0,
        count: 1
      },
      scb: function (res) {
        cb && cb(res)
      }
    }
    this.$http(params)
  }

}

export { Around }


import { Config } from 'config.js'
let QQMapWX = require('qqmap-wx-jssdk.min.js')


let config = new Config()
let mapObj = new QQMapWX({
  key: config.getMapKey()
})

class Base {
  constructor() {
    // 图片访问地址
    this.baseImageHostUrl = config.getBaseImageHostUrl();
    // bmob App
    this.bmobApplicationId = config.getBmobOptions().applicationId;
    // bmob restkey
    this.bmobRestApiKey = config.getBmobOptions().restApiKey;
    // API 地址
    this.baseApiUrl = config.getApiUrl();
    // 上传url
    this.uploadApiUrl = config.getUploadApiUrl();
    // 表情数据
    this.expressionsList = config.getExpressionsIcon();
    // cdnname
    this.cdname = config.getCDNName()
    // map
    this.mapObj = mapObj
  }

  $log(name, data) {
    console.log(name + ':');
    console.log(data)
    console.log('---------------------------------------------------------------');
  }

  $http(params) {

    let baseurl = !!params.contentType ? this.uploadApiUrl : this.baseApiUrl;
    // 删除文件
    if ( params.actionType === 'delfile' ) {
        baseurl = 'https://api.bmob.cn/2/files/' + this.cdname + '/'
    }
    let url = baseurl + params.url;
    let that = this;
    let _header = {
      'content-type': params.contentType || 'application/json',
      'X-Bmob-Application-Id': this.bmobApplicationId,
      'X-Bmob-REST-API-Key': this.bmobRestApiKey
    }
    wx.request({
      url: url,
      data: params.data,
      method: params.type || 'GET',
      header: _header,
      success: function (res) {
        if (res.statusCode >= 200 && res.statusCode < 300) {
          params.scb && params.scb(res.data);  
        } else {
          params.ecb && params.ecb();
        }
      },
      fail: function(err){
        that.$log('err', err);
      }
    })
  }

  // 获取事件对象上的属性
  getDataSet(event, key) {
    return event.currentTarget.dataset[key];
  }

  getWindowHeight(cb){
    let _h = 0
    wx.getSystemInfo({
      success: function (res) {
        _h = res.windowHeight
        cb && cb(_h)
      }
    })
  }

  getattrInStorage(attr, cb) {
    let userid = ''
    wx.getStorage({
      key: attr,
      success: function(res){
        cb && cb(res.data)
      }
    })
  }

  // 从数组中挑出
  findAttrsFromArr(arr, attr) {
    if (Object.prototype.toString.call(arr) === '[object Array]') {
      let result = [];
      for (let i = 0, len = arr.length; i < len; i++) {
        let item = arr[i];
        if (typeof item[attr] !== 'undefined') {
          result.push(item[attr])
        }
      }
      return result;
    } else {
      return false;
    }
  }

  // 提示
  dataLoading(txt, icon, fun) {
    wx.showToast({
      title: txt,
      icon: icon,
      duration: 500,
      success: fun
    })
  }

  // 去掉2边空格
  trim(str) {
    if (Object.prototype.toString.call(str) !== '[object String]') {
      return;
    }
    return str.replace(/(^\s*)|(\s*)$/g, "")
  }

  // 确认弹窗
  confirmModal(title, content, cb) {

    wx.showModal({
      title: title,
      content: content,
      showCancel: false,
      confirmColor: "#a07c52",
      success: function (res) {
        if (res.confirm) {
          cb && cb()
        }
        else {
          
        }
      }
    })
  }

  // 根据attr，从数组中获取attr对应的数据
  getRowByAttrFromArr(arr, attr, attrvalue) {
    if (Object.prototype.toString.call(arr) === '[object Array]') {
       let result = null;
       for (let i = 0, len =  arr.length; i < len; i++) {
          if (arr[i][attr] === attrvalue) {
             result = arr[i];
          }
       }
       return result;
    }
  }

  preview(event) {
    let url = this.getDataSet(event, 'url')
    wx.previewImage({
      current: url,
      urls: [url]
    })
  }

  imageUtil(e) { 
    let imageSize = {}; 
    let originalWidth = e.detail.width;//图片原始宽 
    let originalHeight = e.detail.height;//图片原始高 
    let originalScale = originalHeight/originalWidth;//图片高宽比 
    console.log('originalWidth: ' + originalWidth) 
    console.log('originalHeight: ' + originalHeight) 
      //获取屏幕宽高 
      wx.getSystemInfo({ 
        success: function (res) { 
        let windowWidth = res.windowWidth; 
        let windowHeight = res.windowHeight; 
        let windowscale = windowHeight/windowWidth;//屏幕高宽比 
        //图片高宽比小于屏幕高宽比 
        if(originalScale < windowscale){
          //图片缩放后的宽为屏幕宽 
          imageSize.imageWidth = windowWidth; 
          imageSize.imageHeight = (windowWidth * originalHeight) / originalWidth; 
        }
        //图片高宽比大于屏幕高宽比 
        else{
          //图片缩放后的高为屏幕高 
          imageSize.imageHeight = windowHeight; 
          imageSize.imageWidth = (windowHeight * originalWidth) / originalHeight; 
        } 
        } 
      }) 
      // console.log('缩放后的宽: ' + imageSize.imageWidth) 
      // console.log('缩放后的高: ' + imageSize.imageHeight) 
      return imageSize; 
    }

}


export { Base }

class Config {
  constructor() {
  }

  // Bmob相关参数
  getBmobOptions(){
    return {
      applicationId: '6f8b84bceb4f1f809287cae4ed5a75da',
      restApiKey: 'a1c95eb04f7005f79b5b59432cd90ff3'
    }
  }

  // 图片加载地址
  getBaseImageHostUrl (){
    return 'http://osbfr39w7.bkt.clouddn.com';
  }

  // 接口请求基地址
  getApiUrl () {
    return 'https://api.bmob.cn/1/classes/';
  }

  // 上传请求基地址
  getUploadApiUrl() {
    return 'https://api.bmob.cn/2/files/';
  }

  getCDNName() {
    return 'upyun'
  }

  // 表情数据配置
  getExpressionsIcon() {
    return [
      {
        id: 1,
        url: '../../imgs/expression/cry.png',
        name: 'cry'
      }, {
        id: 2,
        url: '../../imgs/expression/kiss.png',
        name: 'kiss'
      }, {
        id: 3,
        url: '../../imgs/expression/pleased.png',
        name: 'pleased'
      }, {
        id: 4,
        url: '../../imgs/expression/sad.png',
        name: 'sad'
      }, {
        id: 5,
        url: '../../imgs/expression/speechless.png',
        name: 'speechless'
      }, {
        id: 6,
        url: '../../imgs/expression/shy.png',
        name: 'shy'
      }]
  }


  // 地图key
  getMapKey() {
    return '3FPBZ-NWJ3I-OEXGJ-5AJEL-JKDSH-GPFHE'
  }

}

export { Config }
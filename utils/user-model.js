
import { Base } from './base.js'
import { Bmob } from './bmob.js'

class User extends Base{
  constructor() {
    super()
    this.code = '';
    this.openid = '';
    this.userInfo = '';
    this.nickName = '';
    this.avatarUrl = '';
    this.userData = null;
    this.latitude = 0;
    this.longitude = 0;
    this.province_city = '';
  }

  // 微信登录
  wxlogin(cb) {
    let that = this
    wx.login({
      success: (res) => {
        if (res.code) {
           that.code = res.code;
           cb && cb()
        }
      }
    })
  }

  // 跟bmob获取opendid
  getOpenidByBmob(cb) {
    let that = this
    Bmob.User.requestOpenId(that.code, {
      success: function (userData) {
        that.openid = userData.openid;
        that.userData = userData;
        cb && cb()
      }
    })
  }

  // 微信获取用户信息
  wxGetUserInfo(cb) {
    let that = this;
    wx.getUserInfo({
      success: function (result) {
        that.userInfo = result.userInfo
        that.nickName = that.userInfo.nickName
        that.avatarUrl = that.userInfo.avatarUrl
        wx.getLocation({
          type: 'wgs84',
          success: function(res){
            that.latitude = res.latitude
            that.longitude = res.longitude
            cb && cb()
          },
          fail: function(){
            cb && cb()
          }
        })
      }
    })
  }

  // bmob登录
  bmobLogin(cb) {
    let that = this
    Bmob.User.logIn(that.nickName, that.openid, {
      success: function (loginUser) {
        wx.setStorageSync('user_openid', loginUser.get("userData").openid)
        wx.setStorageSync('user_id', loginUser.id);
        wx.setStorageSync('my_nick', loginUser.get("nickname"))
        wx.setStorageSync('my_username', loginUser.get("username"))
        wx.setStorageSync('my_avatar', loginUser.get("userPic"))
        wx.setStorageSync('longitude', loginUser.get("longitude"))
        wx.setStorageSync('latitude', loginUser.get("latitude"))
        wx.setStorageSync('province_city', loginUser.get("province_city"))
        that.user_name = loginUser.get("username");
        that.user_id = loginUser.id;
      },
      error: function (loginUser, error) {
        if (error.code == "101") {
           cb && cb()
        }
      }
    })
  }

  // bmob 注册新用户
  regist(cb) {
    let that = this;
    let new_user = new Bmob.User();
    
    //开始注册用户
    new_user.set("username", that.nickName);

    //因为密码必须提供，但是微信直接登录小程序是没有密码的，所以用openId作为唯一密码    
    new_user.set("password", that.openid);

    new_user.set("nickName", that.nickName);
    new_user.set("userPic", that.avatarUrl);
    new_user.set("userData", that.userData);
    new_user.set('opendid', that.opendid);
    new_user.set('latitude', that.latitude);
    new_user.set('longitude', that.longitude);

    that.mapObj.reverseGeocoder({
      location: {
        latitude: that.latitude,
        longitude: that.longitude
      },
      success: function (res) {
        if (res.status === 0) {
          let province_city = res.result.ad_info.province + res.result.ad_info.city
          new_user.set('province_city', province_city);
          new_user.signUp(null, {
            success: function (results) {
              try {
                that.openid = results.get("userData").openid;
                that.nickName = results.get("nickname");
                that.avatarUrl = results.get("userPic");
                that.user_id = results.id;
                that.user_name = results.get("username");
                that.latitude = results.get("latitude");
                that.longitude = results.get("longitude");
                that.province_city = results.get("province_city");

                //将返回的3rd_session储存到缓存
                wx.setStorageSync('user_openid', that.openid)
                wx.setStorageSync('user_id', that.user_id);
                wx.setStorageSync('my_username', that.user_name);
                wx.setStorageSync('my_nick', that.nickName);
                wx.setStorageSync('my_avatar', that.avatarUrl);
                wx.setStorageSync('latitude', that.latitude);
                wx.setStorageSync('longitude', that.longitude);
                wx.setStorageSync('province_city', that.province_city);
                cb && cb()
              } catch (e) { }
            },
            error: function (userData, error) {
              that.$log('error', error)
            }
          })
        }
      },
      fail: function (res) {
        that.$log('location', res)
      }
    })
  }

}

export { User }